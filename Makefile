start: deps build
	@npm run start

deps:
	@npm install

build:
	@npm run build

dev:
	@npm run serve

docker-push: docker-build
	@docker push eu.gcr.io/fis-map-287815/fish-map:latest

docker-build:
	@docker build -t eu.gcr.io/fis-map-287815/fish-map:latest .

PHONY: start deps build dev docker-push docker-build