resource "google_app_engine_application" "app" {
  project     = var.project
  location_id = var.location_id
}

resource "null_resource" "init_docker_images" {
  provisioner "local-exec" {
    command = "make --directory ../ docker-push"
  }

  depends_on = [google_project_service.container_registry]
}

resource "google_app_engine_flexible_app_version" "v1" {
  version_id = "v1"
  project    = var.project
  service    = "default"
  runtime    = "custom"

  liveness_check {
    path = "/_ah/health"
  }

  readiness_check {
    path = "/"
  }

  manual_scaling {
    instances = 1
  }

  resources {
    cpu       = 1
    memory_gb = 2
    disk_gb   = 10
  }

  deployment {
    container {
      image = "eu.gcr.io/${var.project}/fish-map:latest"
    }
  }

  depends_on = [null_resource.init_docker_images]
}
