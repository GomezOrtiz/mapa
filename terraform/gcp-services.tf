resource "google_project_service" "cloud-resource-manager-api" {
  project = var.project
  service = "cloudresourcemanager.googleapis.com"
}

resource "google_project_service" "appengine-api" {
  project = var.project
  service = "appengine.googleapis.com"
}

resource "google_project_service" "appengineflex" {
  project = var.project
  service = "appengineflex.googleapis.com"
}

resource "google_project_service" "container_registry" {
  project = var.project
  service = "containerregistry.googleapis.com"
}