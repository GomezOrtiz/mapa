provider "google" {
  credentials = file(var.credentials_file)
  project     = var.project
  region      = var.region
  zone        = var.zone
}

resource "google_storage_bucket" "tf-state-bucket" {
  name     = var.bucket_name
  location = var.region

  versioning {
    enabled = true
  }
}