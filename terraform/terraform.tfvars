project = "fis-map-287815"
credentials_file = "gcp_config.json"
location_id = "europe-west"
region = "europe-west1"
zone = "europe-west1-b"
bucket_name = "terraform-state-fis-map"