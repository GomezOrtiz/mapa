terraform {
  backend "gcs" {
    bucket      = "terraform-state-fis-map"
    credentials = "gcp_config.json"
  }
}
